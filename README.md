# morse-leds

morse-leds is a tiny C program that Reads text from stdin and plays it in [International Morse](https://en.wikipedia.org/wiki/Morse_code#/media/File:International_Morse_Code.svg) through a computer's LED or to stdout.

### Usage

```
Usage: morse-led [-u MILLIES] [-l DEVICE] [-p] [-h]

Options:
-u, --unit-millies <millies>  Set milliseconds of a unit (default: 250)
-l, --led-device <device>     Set LED device from /sys/class/leds (default: tpacpi::thinklight)
-p, --print-stdout            Print Morse to stdout instead of a LED
-h, --help                    Print this help
```

### Build

#### Dependencies
- Make
- C compiler

#### Compilation
Just run `make`. If you want debug symbols, run `make debug`

#### Installation
You can directly run the compiled executable, but if you want to install it on your system, `make install` will do it. Notice that you can set the variable `PREFIX` (default to /usr/local) to your desire

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)

### FAQ
- **Why?**
Yes.
