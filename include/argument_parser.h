/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENT_PARSER_H
#define ARGUMENT_PARSER_H

#include <stdbool.h>

#define DEF_UNIT_MILLIS 250
#define DEF_LED_DEVICE "tpacpi::thinklight"
#define DEF_PRINT_TO_STDOUT false

typedef struct
{
	int unit_millis;
	char *led_device;
	bool print_to_stdout;
}
arguments_t;

/*
 * Parses the CLI arguments returning a pointer to an allocated arguments_t
 * filled with the information. Terminates the program in case of error or
 * if the help was printed.
 */
arguments_t *
parse_arguments(int argc, char *argv[]);

/*
 * Frees the memory of an arguments_t.
 */
void
free_arguments(arguments_t *args);

#endif
