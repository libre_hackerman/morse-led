/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <locale.h>
#include <stdio.h>
#include <argument_parser.h>
#include <alphabet.h>

/*
 * Stop thread execution for the specified millies
 */
static void
wait_millis(int millis)
{
	struct timespec ts;

	ts.tv_sec = millis / 1000;
	ts.tv_nsec = (millis % 1000) * 1000000;

	nanosleep(&ts, NULL);
}

/*
 * Turns on a LED given its /sys/class/leds/XXXXX/brightness file during the
 * specified milliseconds
 */
static void
led_pulse(char *brightness_file, int millis)
{
	FILE *f;

	if (!(f = fopen(brightness_file, "w")))
	{
		perror(brightness_file);
		return ;
	}
	fputs("255\n", f);
	fclose(f);

	wait_millis(millis);

	if (!(f = fopen(brightness_file, "w")))
	{
		perror(brightness_file);
		return ;
	}
	fputs("0\n", f);
	fclose(f);
}

/*
 * Plays a letter and waits three units in Morse
 */
static void
play_letter(char c, arguments_t *args)
{
	char *morse;
	char *brightness_file;

	/* Get morse string equivalent to c */
	if (isalpha(c))
	{
		c = tolower(c);
		morse = alphabet[c - 'a'];
	}
	else if (isdigit(c))
		morse = numbers[c - '0'];
	else
		return ;

	/* Generate brightness file path */
	if (!(brightness_file = calloc(sizeof(char),
			snprintf(NULL, 0, "/sys/class/leds/%s/brightness", args->led_device)
			+ 1)))
	{
		perror("Can't allocate memory for LED file");
		return ;
	}
	sprintf(brightness_file, "/sys/class/leds/%s/brightness", args->led_device);

	while (*morse)
	{
		if (*morse == '.')
			led_pulse(brightness_file, args->unit_millis);
		else
			led_pulse(brightness_file, 3 * args->unit_millis);
		wait_millis(args->unit_millis);
		morse++;
	}
	wait_millis(3 * args->unit_millis);

	free(brightness_file);
}

/*
 * Reads the text received from stdin and plays it in Morse acording to the
 * CLI arguments
 */
static void
play_morse(arguments_t *args)
{
	char c;

	while ((c = fgetc(stdin)) != EOF)
	{
		if (isalnum(c))
			play_letter(c, args);
		else if (c == ' ' || c == '\n')
			wait_millis(4 * args->unit_millis);
		else
			fprintf(stderr, "Character without International Morse equivalent: '%c'\n", c);
	}
}

/*
 * Reads the text received from stdin and prints it in Morse to stdout
 */
static void
print_morse()
{
	char c;

	while ((c = fgetc(stdin)) != EOF)
	{
		if (isalpha(c))
		{
			c = tolower(c);
			printf("%s   ", alphabet[c - 'a']);
		}
		else if (isdigit(c))
			printf("%s   ", numbers[c - '0']);
		else if (c == ' ' || c == '\n')
			printf("    ");
		else
			fprintf(stderr, "Character without International Morse equivalent: '%c'\n", c);
	}
	puts("");
}

int
main(int argc, char *argv[])
{
	arguments_t *args;

	setlocale(LC_ALL, "C");
	args = parse_arguments(argc, argv);
	if (args->print_to_stdout)
		print_morse();
	else
		play_morse(args);
	free_arguments(args);

	return (EXIT_SUCCESS);
}
