/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <argument_parser.h>

/*
 * Prints the help
 */
static void
print_help()
{
	int opt_width = 30;

	puts("Reads text from stdin and plays it in Morse through a computer's LED");
	puts("\nUsage: morse-led [-u MILLIES] [-l DEVICE] [-p] [-h]");
	puts("\nOptions:");

	printf("%-*sSet milliseconds of a unit (default: %d)\n", opt_width,
			"-u, --unit-millies <millies>", DEF_UNIT_MILLIS);
	printf("%-*sSet LED device from /sys/class/leds (default: %s)\n", opt_width,
			"-l, --led-device <device>", DEF_LED_DEVICE);
	printf("%-*sPrint Morse to stdout instead of a LED\n", opt_width,
			"-p, --print-stdout");
	printf("%-*sPrint this help\n", opt_width, "-h, --help");
}

/*
 * Returns a pointer to an allocated arguments_t filled with the default
 * values. Returns NULL in case of error.
 */
static arguments_t *
init_arguments()
{
	arguments_t *args;

	if (!(args = malloc(sizeof(arguments_t))))
	{
		perror("Can't allocate CLI arguments");
		return (NULL);
	}

	args->unit_millis = DEF_UNIT_MILLIS;
	args->led_device = strdup(DEF_LED_DEVICE);
	args->print_to_stdout = DEF_PRINT_TO_STDOUT;

	return (args);
}

/*
 * See include/argument_parser.h
 */
arguments_t *
parse_arguments(int argc, char *argv[])
{
	arguments_t *args;
	int op;

	if (!(args = init_arguments()))
		exit(EXIT_FAILURE);

	struct option long_options[] = {
		{"unit-millies", required_argument, NULL, 'u'},
		{"led-device", required_argument, NULL, 'l'},
		{"print-stdout", no_argument, NULL, 'p'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};
	while ((op = getopt_long(argc, argv, ":u:l:ph", long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'u':
				if (isdigit(*optarg))
					args->unit_millis = atoi(optarg);
				else
				{	
					fprintf(stderr, "%s is not a number\n", optarg);
					free_arguments(args);
					exit(EXIT_FAILURE);
				}
				break;
			case 'l':
				free(args->led_device);
				args->led_device = strdup(optarg);
				break;
			case 'p':
				args->print_to_stdout = true;
				break;
			case 'h':
				print_help();
				free_arguments(args);
				exit(EXIT_SUCCESS);
				break;
			case ':':
				fprintf(stderr, "Missing required argument\n");
				free_arguments(args);
				exit(EXIT_FAILURE);
				break;
			case '?':
				fprintf(stderr, "Unknown argument\n");
				free_arguments(args);
				exit(EXIT_FAILURE);
				break;
		}
	}

	return (args);
}

/*
 * See include/argument_parser.h
 */
void
free_arguments(arguments_t *args)
{
	if (args)
	{
		free(args->led_device);
		free(args);
	}
}
